<?php get_header(); ?> 
<div id="primary"> 
	<div class="top-text"> 
		<? printf( __( 'Результаты поиска: %s'), '&#8217;' . get_search_query() . '&#8217;' ); ?> 
	</div> 
	<?php get_template_part( 'content', get_post_format() ); ?> 
</div> 
<?php get_footer(); ?>