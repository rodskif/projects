<?php get_sidebar(); ?> 
<div class="clear"></div> 
<div id="footer"> 
<div id="copyright"> &copy; 2017 - 
<?php echo date('Y'); ?> rodskif.com (Yura Melnik) Лучший слоган в мире. Все права защищены. Любое использование материалов допускается только с указанием активной ссылки на источник. 
</div> 
<div id="metrika"> Сюда код счетчиков или метрики </div> 
<div class="clear"></div> 
</div><!--Закрываем footer--> 
</div><!--Закрываем main--> 
<?php wp_footer(); ?> 
</body> 
</html>