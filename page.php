<?php get_header(); ?> 
<div id="primary"> 
	<div class="entry-breadcrumb">
		<?php the_breadcrumbs() ?>
	</div> 
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
		<div class="content_block"> 
			<h1 class="post_title"><?php the_title(); ?></h1> 
			<div class="content_text"><?php the_content(); ?></div> 
			<?php edit_post_link(__('Редактировать')); ?> 
		</div> 
	<?php endwhile; else: ?> 
	<?php include(TEMPLATEPATH . '/404.php');?> 
	<?php endif; ?> 
</div> 
<?php get_footer(); ?>