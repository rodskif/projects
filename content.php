<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?> 
<div class="content_block"> <?php the_post_thumbnail(); ?> 
	<h1 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1> 
	<ul class="dop_info"> 
		<li><?php echo get_the_date("d M Yг"); ?></li> 
		<li>Рубрика: <?php the_category(', ') ?></li> 
	</ul> 
	<div class="content_text"><?php the_excerpt()?></div> 
		<a class="more_link" href="<?php the_permalink() ?>">Читать дальше</a> 
</div> 
<?php endwhile; ?> 
<?php if (function_exists('wp_corenavi')) wp_corenavi(); ?> 
<?php else : ?> 
<?php include(TEMPLATEPATH . '/404.php');?> 
<?php endif; ?>