<?php 
//мета теги
function add_head_meta_tags()
{
    ?>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png" type="image/x-icon">
	<meta charset="UTF-8">
	<meta name="keywords" content="blog, wordpress, тема">
	<meta name="description" content="Наш сайт самый сайтовый сайт и накакой сайт не пересайтит по сайтости наш сайт!">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');

// Підключаємо css файли
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    //wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    //wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );
//Меню 
function register_my_menus() { 
	register_nav_menus ( 
		array('top-menu' => 'Меню в шапке') 
	); 
} 
if (function_exists('register_nav_menus')) { add_action( 'init', 'register_my_menus' ); 
} 

//Крошки 
function the_breadcrumbs() { 
	echo ''; 
	if (!is_front_page()) { 
		echo '<a href="'; 
		echo get_option('home'); 
		echo '">Главная'; 
		echo '</a> &raquo; ';
		if (is_category() || is_single()) { 
			the_category(' '); 
			if (is_single()) { 
				echo ' &raquo; '; 
				the_title(); 
			} 
		} 
		elseif (is_page()) { 
			echo the_title(); 
		} 
	} 
	else { 
		echo 'Последние записи'; 
	} 
}

// поддержка миниатюр 
add_theme_support('post-thumbnails'); 
set_post_thumbnail_size(180, 180, false);

// определение количества слов для тизера
function new_excerpt_length($length) { 
	return 25; 
} 
add_filter('excerpt_length', 'new_excerpt_length');

//Убираем ... в тизере 
function new_excerpt_more($more) { 
	global $post; 
	return '';
}

//номерация страниц внизу 
function wp_corenavi() { 
	global $wp_query, $wp_rewrite; 
	$pages = ''; 
	$max = $wp_query->max_num_pages; 
	if (!$current = get_query_var('paged')) $current = 1; 
	$a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999)); 
	$a['total'] = $max; 
	$a['current'] = $current; 
	$total = 1; //1 - выводить текст "Страница N из N", 0 - не выводить 
	$a['mid_size'] = 3; //сколько ссылок показывать слева и справа от текущей 
	$a['end_size'] = 1; //сколько ссылок показывать в начале и в конце 
	$a['prev_text'] = 'Назад;'; //текст ссылки "Предыдущая страница" 
	$a['next_text'] = 'Дальше'; //текст ссылки "Следующая страница" 
	if ($max > 1) echo '<div class="navigator">'; 
	if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n"; 
	echo $pages . paginate_links($a); 
	if ($max > 1) echo '</div>'; 
} 

//Сайдбары 
if ( function_exists('register_sidebar') ) 
	register_sidebar(array( 
		'name' => 'Правый сайдбар', 
		'before_widget' => '', 
		'after_widget' => '', 
		'before_title' => '<div id="titlebg">', 
		'after_title' => '</div>', 
	));

?>