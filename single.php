<?php get_header(); ?> 
<div id="primary"> 
	<div class="entry-breadcrumb">
		<?php the_breadcrumbs() ?>
	</div> 
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?> 
		<div class="content_block"> 
			<?php the_post_thumbnail(); ?> 
			<h1 class="post_title">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
					<?php the_title(); ?>
				</a>
			</h1> 
			<ul class="dop_info"> 
				<li><?php echo get_the_date("d M Yг"); ?></li> 
				<li>Рубрика: <?php the_category(', ') ?></li> 
			</ul> 
			<div class="content_text">
				<?php the_content(); ?>
			</div> 
			<div class="tags">
				<?php the_tags(); ?>
			</div> 
			<?php edit_post_link(__('Редактировать')); ?> 
			<?php comments_template(); ?> 
		</div> 
	<?php endwhile; else: ?> 
	<?php include(TEMPLATEPATH . '/404.php');?> 
	<?php endif; ?> 
</div> 
<?php get_footer(); ?>