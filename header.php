<!DOCTYPE html>
<html lang="ru-Ru">
<!-- <html <?php language_attributes(); ?>> //динамическое определенние языка-->
<head>
	<!-- <title>Блог</title> //статическое название -->
<!-- Динамический title. Он будет меняться в зависимости от того, на какой странице Вы находитесь. То есть он будет выводить название страницы, а после название сайта. -->
	<title>
		<?php 
		global $page, $paged;
		wp_title('|', true, 'right');
		bloginfo('name');
		$site_description = get_bloginfo('description', 'display');
		if ($site_description && (is_home() || is_front_page()))
			echo " | $site_description";
		if ($paged >= 2 || $page >= 2 )
			echo ' | '.sprintf(__('Page %s', 'striped'), max($paged, $page));
		 ?>
	</title>
	<!-- <meta charset="UTF-8"> -->
	<!-- <meta name="keywords" content="blog, wordpress, тема"> -->
	<!-- <meta name="description" content="Наш сайт самый сайтовый сайт и накакой сайт не пересайтит по сайтости наш сайт!"> -->
	<!-- <link rel="stylesheet" type="text/css" media="all" href="wp-content/themes/mytheme/style.css"> -->

	<?php wp_head(); ?>
</head>
<body>
	<div id="main">
		<div id="header">
			<div id="logo"></div>
			<div class="desc"><?php bloginfo('description'); ?></div>
			<div id="search"><?php get_search_form(); ?></div>
			<div id="menu"><?php wp_nav_menu( array('theme_location' => 'top-menu') ); ?></div>
		</div>